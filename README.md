# bytesweep-go

ByteSweep is a security analysis tool for IoT firmware. This is a complete rewrite of the original ByteSweep project in Go.

# Getting Started

Download the ByteSweep source code into your [workspace](https://golang.org/doc/code.html#GOPATH)

```
git clone https://gitlab.com/bytesweep/bytesweep-go.git "$GOPATH/src/gitlab.com/bytesweep/bytesweep-go"
```

Initialize submodule dependencies and build

```
git submodule update --init
go get
go build
```

This will compile the ByteSweep program. For help running ByteSweep, execute `./bytesweep-go -h`
