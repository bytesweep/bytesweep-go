package regexp

import (
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
)

func getStrings(filename string, minLen int) []String {
	// this function needs to return a byte offset value associated with every string
	var strList []String

	file, err := os.Open(filename)
	if nil != err {
		fmt.Println(err)
		return strList
	}
	defer file.Close()

	data := make([]byte, 4096)
	var str string
	var offset int

	for {
		data = data[:cap(data)]
		count, err := file.Read(data)
		if nil != err {
			if io.EOF == err {
				break
			}
			fmt.Println(err)
			return strList
		}
		data = data[:count]
		for _, piece := range data {
			if '\x20' <= piece && '\x7e' >= piece {
				str += string(piece)
			} else {
				if len(str) >= minLen {
					strList = append(strList, String{value: str, offset: offset})
				}
				str = ""
			}
			offset++
		}
	}
	if len(str) > minLen {
		strList = append(strList, String{value: str, offset: offset})
	}

	return strList
}

type String struct {
	value  string
	offset int
}

type Pattern struct {
	name        string
	pattern     string
	description string
}

type Match struct {
	match           string
	matchOffset     int
	rawString       string
	rawStringOffset int
	pattern         Pattern
}

type RegexSearchResult struct {
	filename string
	matches  []Match
}

func regexSearch(regexSearchResultsChannel chan RegexSearchResult, filename string, patterns []Pattern) {
	var matches []Match

	strList := getStrings(filename, 2)
	for _, pattern := range patterns {
		re := regexp.MustCompile(pattern.pattern)

		for _, str := range strList {
			strMatches := re.FindAllString(str.value, -1)
			for _, strMatch := range strMatches {
				var match Match
				match.match = strMatch
				match.rawString = str.value
				match.rawStringOffset = str.offset
				match.pattern = pattern
				matchOffset := strings.Index(match.rawString, match.match)
				match.matchOffset = match.rawStringOffset
				if 0 < matchOffset {
					match.matchOffset += matchOffset
				}
				matches = append(matches, match)
			}
		}
	}
	regexSearchResultsChannel <- RegexSearchResult{filename: filename, matches: matches}
}

func main() {
	/* Define the patterns we want to search for */
	patterns := []Pattern{
		Pattern{
			name:    "unix-passwd",
			pattern: `[0-9a-zA-Z]+:[$0-9a-zA-Z.\/]{5,}:[0-9]*:[0-9]*:[0-9a-zA-Z\s]*:[0-9a-zA-Z.\/\-\_]*:[0-9a-zA-Z.\/\-\_]*$`},
		Pattern{
			name:    "unix-shadow",
			pattern: `[0-9a-zA-Z]+:[$0-9a-zA-Z.\/]{5,}:[0-9]*:[0-9]*:[0-9]*:[0-9]*:[0-9]*:[0-9]*:$`},
	}

	filenames := []string{"tests/passwd.txt", "tests/shadow.txt"}

	/* Dispatch goroutines that will search for the specified regular expression pattern
	in the specified file, then return matches using the results channel */
	regexSearchResultsChannel := make(chan RegexSearchResult)
	for _, filename := range filenames {
		go regexSearch(regexSearchResultsChannel, filename, patterns)
	}

	/* Wait until results come through over the channel from any of the goroutines,
	then process/print the results */
	for i := 0; len(filenames) > i; i++ {
		regexSearchResult := <-regexSearchResultsChannel
		filename := regexSearchResult.filename
		matches := regexSearchResult.matches

		/* Print an appropriate header based on the number of matches we got */
		if 1 == len(matches) {
			fmt.Println(strings.Repeat("-", len(filename)+len("1 match found")))
			fmt.Println(filename, "1 match found\n")
		} else {
			fmt.Println(strings.Repeat("-", len(filename)+len(matches)+len(" matches found")))
			fmt.Println(filename, len(matches), "matches found\n")
		}

		/* Loop through the matches and print their details */
		for matchNumber, match := range matches {
			fmt.Printf("#%d\n", matchNumber+1)
			fmt.Printf("\t%-25.25s %s\n", "Pattern", match.pattern.pattern)
			fmt.Printf("\t%-25.25s %s\n", "Match", match.match)
			fmt.Printf("\t%-25.25s %s\n", "Raw String", match.rawString)
			fmt.Printf("\t%-25.25s %d\n", "Raw String Offset", match.rawStringOffset)
			fmt.Printf("\t%-25.25s %d\n\n", "Match Offset", match.matchOffset)
		}
	}
}
