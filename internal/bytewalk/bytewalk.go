package bytewalk

import (
	"os/exec"
)

func Extract(filepath string) (string, error) {
	cmd := exec.Command("bytewalk", "-e", "-M", filepath)
	stdout, err := cmd.Output()
	if nil != err {
		return string(stdout), err
	}

	return string(stdout), nil
}
