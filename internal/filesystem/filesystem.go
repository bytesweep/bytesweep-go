package filesystem

import (
	"crypto/sha1"
	"errors"
	"fmt"
	"github.com/rakyll/magicmime"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
)

type Node struct {
	name     string
	path     string
	size     int64
	hashes   map[string]string
	magic    string
	isdir    bool
	isfile   bool
	children []Node
}

func hashSha1(filename string) (string, error) {
	file, err := os.Open(filename)
	if nil != err {
		return "", err
	}
	defer file.Close()

	hasher := sha1.New()
	_, err = io.Copy(hasher, file)
	if nil != err {
		return "", err
	}

	return fmt.Sprintf("%x", hasher.Sum(nil)), nil
}

func GetNode(path string) (Node, error) {
	var n Node
	var err error

	fstat, err := os.Stat(path)
	if nil != err {
		return n, err
	}
	n.name = fstat.Name()
	n.path = path
	n.size = fstat.Size()
	n.magic = ""
	n.hashes = make(map[string]string)
	n.isdir = fstat.Mode().IsDir()
	n.isfile = fstat.Mode().IsRegular()
	n.children = nil

	return n, nil
}

func getMagic(filepath string) (string, error) {
	var mimetype string
	var err error

	err = magicmime.Open(magicmime.MAGIC_MIME_TYPE | magicmime.MAGIC_SYMLINK | magicmime.MAGIC_ERROR)
	if nil != err {
		return "", err
	}
	defer magicmime.Close()

	mimetype, err = magicmime.TypeByFile(filepath)
	if nil != err {
		return "", err
	}

	return mimetype, nil
}

func WalkFilesystem(root Node) (Node, error) {
	fstat, err := os.Stat(root.path)
	if nil != err {
		return Node{}, fmt.Errorf("File stat error: %s", root.name)
	}

	if false == fstat.Mode().IsDir() {
		return Node{}, errors.New("Not a directory")
	}

	nodes, err := ioutil.ReadDir(root.path)
	if nil != err {
		return Node{}, fmt.Errorf("Directory list error: %s", root.name)
	}

	var nodeList []Node
	for _, node := range nodes {
		n, err := GetNode(filepath.Join(root.path, node.Name()))
		if nil != err {
			return Node{}, err
		}

		if true == n.isdir {
			n, err = WalkFilesystem(n)
			if nil != err {
				return Node{}, err
			}
		} else if true == n.isfile {
			n.magic, err = getMagic(n.path)
			if nil != err {
				continue
			}

			n.hashes["sha1"], err = hashSha1(n.path)
			if nil != err {
				return Node{}, err
			}
		}
		nodeList = append(nodeList, n)
	}
	root.children = nodeList

	return root, nil
}
