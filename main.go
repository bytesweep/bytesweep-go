package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/bytesweep/bytesweep-go/internal/bytewalk"
	"gitlab.com/bytesweep/bytesweep-go/internal/filesystem"

	"gitlab.com/bytesweep/bytesweep-go/external/progressbar"
)

func check(err error) {
	if nil != err {
		/* Print an extra newline to break out from
		carriage return '\r' printed by the progress bar
		This will ensure the `panic()` is printed neatly
		starting on its own line */
		fmt.Println()
		panic(err)
	}
}

func main() {
	target := flag.String("file", "", "path to the file to analyze")
	flag.Parse()

	if "" == *target {
		flag.Usage()
		os.Exit(1)
	}

	progress.Bar(1, 7, "Starting Bytesweep")

	/* Use this current system time to create a unique output directory each
	time Bytesweep is run */
	now := time.Now()

	/* Create the directory where artifacts from bytewalk processing will be stored */
	bytewalkOutputDir := "bytewalk-analysis-" + now.Format(time.RFC3339)
	err := os.Mkdir(bytewalkOutputDir, 0755)
	check(err)

	progress.Bar(2, 7, "Starting Bytesweep")

	/* Create a symbolic link to the input file in the results directory */
	targetSymbolicLink := filepath.Join(bytewalkOutputDir, *target)
	targetAbsolute, err := filepath.Abs(*target)
	check(err)

	progress.Bar(3, 7, "Starting Bytesweep")

	err = os.Symlink(targetAbsolute, targetSymbolicLink)
	check(err)

	/* Run Bytewalk on the symbolic link copy located in the results directory */
	progress.Bar(4, 7, "Running Bytewalk Extraction")
	_, err = bytewalk.Extract(targetSymbolicLink)
	check(err)

	/* Start post-processing the Bytewalk output */
	progress.Bar(5, 7, "Enriching Extracted Artifacts")
	root, err := filesystem.GetNode(filepath.Join(bytewalkOutputDir, "_"+*target+".extracted"))
	check(err)

	progress.Bar(6, 7, "Enriching Extracted Artifacts")
	root, err = filesystem.WalkFilesystem(root)
	check(err)

	progress.Bar(7, 7, "Bytesweep Complete")

	os.Exit(0)
}
